<?php

/**
 * @file
 * Admin page callback file for the google_place_autocomplete module.
 */

/**
 * Form builder; Configure google_place_autocomplete settings for this site.
 *
 * @ingroup forms
 *
 * @see system_settings_form()
 */
function google_place_autocomplete_admin_settings() {
  $form['google_place_autocomplete_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Google API key'),
    '#description' => t('You can get it from your <a href="https://code.google.com/apis/console">Google Console</a>.'),
    '#default_value' => variable_get('google_place_autocomplete_key', ''),
  );

  $documentation_link = variable_get('google_place_autocomplete_documentation_link');
  $options = variable_get('google_place_autocomplete_options', google_place_autocomplete_get_default_options());

  $form['google_place_autocomplete_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Places API parameters'),
    '#tree' => TRUE,
    '#description' => t('The values for the parameters will be used when the autocomplete path is used outside of the scope of a field widget (I.E.: FAPI autocomplete path). If you are using the field widget, these settings will have no effect. Documentation about the parameters, can be found <a target="_blank" href="@url">here</a>.', array('@url' => $documentation_link)),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['google_place_autocomplete_options']['offset'] = array(
    '#type' => 'textfield',
    '#title' => t('Offset'),
    '#default_value' => $options['offset'],
  );
  $form['google_place_autocomplete_options']['location'] = array(
    '#type' => 'textfield',
    '#title' => t('Location'),
    '#default_value' => $options['location'],
  );
  $form['google_place_autocomplete_options']['radius'] = array(
    '#type' => 'textfield',
    '#title' => t('Radius'),
    '#default_value' => $options['radius'],
  );
  $form['google_place_autocomplete_options']['language'] = array(
    '#type' => 'textfield',
    '#title' => t('Language'),
    '#default_value' => $options['language'],
  );
  $form['google_place_autocomplete_options']['types'] = array(
    '#type' => 'textfield',
    '#title' => t('Types'),
    '#default_value' => $options['types'],
  );
  $form['google_place_autocomplete_options']['components'] = array(
    '#type' => 'textfield',
    '#title' => t('Components'),
    '#default_value' => $options['components'],
  );
  $form['google_place_autocomplete_options']['minlength'] = array(
    '#type' => 'textfield',
    '#title' => t('Minimum Query Length'),
    '#default_value' => isset($settings['google_place_autocomplete_options']['minlength']) ? $settings['google_place_autocomplete_options']['minlength'] : $options['minlength'],
  );

  return system_settings_form($form);
}
